#include <stdio.h>

int data_array[] = {5, 6, 8, 1, 3, 2, 9, 7, 0, 4};

void PrintArray(int *array, int size)
{
	int i = 0;
    
	for(i=0; i<size; i++)
	{
		printf("%d ", array[i]);
	}
	printf("\n");
}

void BubbleSort(int *data, int size)
{
    int i = 0;
    int j = 0;
    
    int temp;
    
    for(i=0; i<size-1; i++)
    {
        for(j=0; j<size-i-1; j++)
        {
            if(data[j] > data[j+1])
            {
                temp = data[j+1];
                data[j+1] = data[j];
                data[j] = temp;
            }
        }
    }
}

int main()
{
    BubbleSort(data_array, sizeof(data_array)/sizeof(data_array[0]));
	
    printf("Finish bubble sort\n");
    PrintArray(data_array, sizeof(data_array)/sizeof(data_array[0]));
    
    return 0;
}